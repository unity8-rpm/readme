**PACKAGE**|**COMPILE**|**FOLLOWS GUIDELINES**|**STAGE 1**|**STAGE 2**|**STAGE 3**
:-----:|:-----:|:-----:|:-----:|:-----:|:-----:
apparmor|X|Y| | | 
click|X|Y| | | 
cmake-extras|Y|N| | | 
dbus-cpp|Y|Y| | | 
dbus-test-runner|Y|Y| | | 
gmenuharness|Y| | | | 
gsettings-qt|X| | | | 
gsettings-ubuntu-touch-schemas|Y| | | | 
history-service|X| | | | 
libapparmor|Y| | | | 
libertine|Y| | | | 
libqtdbusmock|Y| | | | 
libqtdbustest|Y| | | | 
mir|Y| | | | 
net-cpp|Y| | | | 
pam-config|Y| | | | 
process-cpp|Y| | | | 
systemd-rpm-macros|Y| | | | 
ubuntu-app-launch|Y| | | | 
ubuntu-download-manager|X| | | | 
unity-api|Y| | | | 
unity-scopes-api|Y| | | | 